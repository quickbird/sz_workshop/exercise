//  
//  TranslatorViewModel.swift
//  lead
//
//  Created by Stefan Kofler on 10.04.18.
//  Copyright © 2018 Zeppelin GmbH. All rights reserved.
//

import Foundation
import RxSwift

class TranslatorViewModelImpl: TranslatorViewModel {

    // MARK: - Inputs
    var inputEnglishText = BehaviorSubject<String>(value: "")
    var inputSaveTrigger = PublishSubject<Void>()

    // MARK: - Outputs

    lazy var outputGermanText: Observable<String> =
        inputEnglishText
            .map(TranslatorEngine.translateToGerman)

    lazy var outputIsSavingAllowed: Observable<Bool> =
        inputEnglishText
            .map { english in !english.isEmpty }

    lazy var outputSavedGermanTranslation: Observable<String> =
        inputSaveTrigger
            .withLatestFrom(outputGermanText)

}
