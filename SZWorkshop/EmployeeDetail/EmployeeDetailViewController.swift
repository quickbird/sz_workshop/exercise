//  
//  EmployeeDetailViewController.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EmployeeDetailViewController: UIViewController {
    var viewModel: EmployeeDetailViewModel!

    @IBOutlet private var titleLabel: UILabel!

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.title
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}
