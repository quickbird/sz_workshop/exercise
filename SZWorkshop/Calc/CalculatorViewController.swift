//
//  CalculatorViewController.swift
//  Schultopf
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift

class CalculatorViewController: UIViewController {
    var viewModel: CalculatorViewModel!

    @IBOutlet private var summandOneField: UITextField!
    @IBOutlet private var summandTwoField: UITextField!
    @IBOutlet private var resultLabel: UILabel!

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Inputs
        summandOneField.rx.text.orEmpty
            .map { Int($0) ?? 0 }
            .bind(to: viewModel.summandOne)
            .disposed(by: disposeBag)

        summandTwoField.rx.text.orEmpty
            .map { Int($0) ?? 0 }
            .bind(to: viewModel.summandTwo)
            .disposed(by: disposeBag)

        // Outputs
        viewModel.sum
            .map { String($0) }
            .bind(to: resultLabel.rx.text)
            .disposed(by: disposeBag)
    }

}
