//
//  TableViewModel.swift
//  Schultopf
//
//  Created by Stefan Kofler on 06.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol EmployeesViewModel {
    // Input
    var inputFilterText: BehaviorSubject<String> { get }

    // Output
    var outputEmployees: Observable<[String]> { get }
    var outputIsLoading: Observable<Bool> { get }

    func detailViewModel(for employee: String) -> EmployeeDetailViewModel
}
