//
//  EmployeesViewModelImpl.swift
//  SZWorkshop
//
//  Created by Stefan Kofler on 09.06.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation
import RxSwift

class EmployeesViewModelImpl: EmployeesViewModel {

    // Private
    private let employeeService: EmployeeService

    init(employeeService: EmployeeService = EmployeeService()) {
        self.employeeService = employeeService
    }

    // Inputs
    let inputFilterText = BehaviorSubject(value: "")

    // Outputs
    lazy var outputEmployees = Observable.combineLatest(inputFilterText, employeeService.getEmployees())
        .map { filterText, allEmployees -> [String] in
            guard !filterText.isEmpty else { return allEmployees }

            return allEmployees.filter { $0.contains(filterText) }
    }

    lazy var outputIsLoading = outputEmployees
        .map { _ in false }
        .startWith(true)

    // Child view models
    func detailViewModel(for employee: String) -> EmployeeDetailViewModel {
        return EmployeeDetailViewModelImpl(employee: employee)
    }
}
